let books = [
    {
        id: 1,
        title: 'Война и мир',
        author: 'Лев Толстой'
    }
];

class BookController {
    getBook(req, res) {
        let book = books.filter(book => book.id === req.swagger.params.id.value);
        if (book && book.length) {
            return res.json(book[0]);
        }
        return res.status(404).json({message: 'Book not found'});
    }
    getAllBooks(req, res) {
        //console.log(books);
        return res.json({data: books});
    }
    createBook(req, res) {
        let { title, author } = req.swagger.params.book.value;
        if (!title) {
            return res.status(400).json({message: 'Required field title is not provided'});
        }
        if (!author) {
            return res.status(400).json({message: 'Required field author is not provided'});
        }

        books.push({
            id: books.length + 1,
            title: title,
            author: author
        });

        return res.json({message: 'Book successfully created'});
    }
    updateBook(req, res) {
        let updated = false;
        let { id, title, author } = req.swagger.params.book.value;
        if (!id) {
            return res.status(400).json({message: 'Required param id is not provided'});
        }

        for (let book of books ) {
            //console.log(typeof id, id);
            if (!updated && book.id === id) {
                updated = true;
                book.author = author ? author : book.author;
                book.title = title ? title : book.title;
            }
        }

        if (updated) {
            res.json({message: `book with id ${id} has been successfully updated`})
        } else {
            res.status(404).json({message: `book with id ${id} is not found`})
        }
    }
    deleteBook(req, res) {
        let id = req.swagger.params.id.value;
        let deleteBookFlag = false;
        if (!id) {
            return res.status(400).json({message: 'Required param id is not provided'});
        }

        books.forEach((book, key, arr) => {
            if (!deleteBookFlag && book.id === id) {
                deleteBookFlag = true;
                arr.splice(key, 1);
            }
        });

        if (deleteBookFlag) {
            return res.json({message: `Book with id ${id} has been successfully deleted`});
        } else {
            return res.status(404).json({message: `Book with id ${id} is not found`});
        }
    }
}

const bookController = new BookController();

module.exports = {
  getAll: bookController.getAllBooks,
  find  : bookController.getBook,
  create: bookController.createBook,
  delete: bookController.deleteBook,
  update: bookController.updateBook,
};